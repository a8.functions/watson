'use strict'
const axios = require('axios')
const Promise = require('bluebird')

const sentimentAnalysis = (urlInput, apiKey) => {
  let urlArray
  if(Array.isArray(urlInput)){
    urlArray = urlInput
  }else{
    urlArray = [urlInput]
  }

  return Promise.map(urlArray, sentimentAnalysisUrl(apiKey))
}

const sentimentAnalysisUrl = (apiKey) => (url) => {
  const apiUrl = `https://gateway.watsonplatform.net/natural-language-understanding/api/v1/analyze?version=2018-11-16`
  const data = {
    url,
    features: {
      sentiment: {}
    }
  }

  const config = {
     auth: {
     username: 'apikey',
     password: apiKey
    }
  }

  return axios.post(apiUrl , data, config).then((result) => {
    return  result.data.sentiment.document.score
  }).catch((e)=>{
    return 0
  })
}

module.exports = sentimentAnalysis
