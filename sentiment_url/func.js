const fdk = require('@autom8/fdk');
const a8 = require('@autom8/js-a8-fdk');
const sentimentAnalysis = require('./sentimentAnalysis.js')

fdk.handle(function(input){
  urls = getURLsFromInputs(input)
  if(!urls){
    return {'message' : 'Please include some URLs'}
  }

  if(!input.apiKey){
    return {'message' : 'Please include an apiKey'}
  }

  return sentimentAnalysis(urls, input.apiKey).then((outputArray)=>{
      return { "sentiments":outputArray}
  })
})

function getURLsFromInputs(input){
  let urls

  if( input._inputs ){
    urls = input._inputs[0]
  }

  if (input.urls) {
    urls = input.urls
  }

  return urls
}

fdk.slack(function(result){
  const emoji = result.sentiment[0]*1 > 0 ? ':thumbsup:' : ':thumbsdown:'
  return {
    "response_type": "in_channel",
    "blocks" : [
        {
          "type": "section",
          "text": {
            "type": "mrkdwn",
            "text": `The sentiment of the news article is: ${result.sentiment[0]} ${emoji}`
          }
        }
      ]
  }
})

fdk.discord(function(result){
  return {
    "content" : `The sentiment of the news article on a scale is: ${result.sentiment}`
  }
})
