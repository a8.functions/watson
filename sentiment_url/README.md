# sentiment_url

Check sentiment of a website or array of websites

## Examples

### CLI

`a8 invoke watson.sentiment_url '{"apiKey":"","urls":"https://money.cnn.com/2011/10/21/technology/techstars/index.htm"}'`

## Input

`apiKey` is a Watson API key

``` json
{
    apiKey : string,
    urls: [array]
}
```

## Output

sentiments is an array of sentiments (from -1 to 1)

``` json
{
    sentiments : [array]
}
```