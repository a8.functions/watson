const fdk = require('@autom8/fdk');
const a8 = require('@autom8/js-a8-fdk');
const sentimentAnalysis = require('./sentimentAnalysis.js')


fdk.handle(function(input){
  text = getTextFromInputs(input)
  if(!text){
    return {'message' : 'Please include some text'}
  }

  if(!input.apiKey){
    return {'message' : 'Please include an apiKey'}
  }

  return sentimentAnalysis(text, input.apiKey).then((outputArray)=>{
      return { "sentiments":outputArray}
  })
})

function getTextFromInputs(input){
  let text

  if( input._inputs ){
    text = input._inputs[0]
  }

  if (input.text) {
    text = input.text
  }

  return text
}

fdk.slack(function(result){
  const emoji = result.sentiment[0]*1 > 0 ? ':thumbsup:' : ':thumbsdown:'
  return {
    "response_type": "in_channel",
    "blocks" : [
        {
          "type": "section",
          "text": {
            "type": "mrkdwn",
            "text": `The sentiment of the news article is: ${result.sentiment[0]} ${emoji}`
          }
        }
      ]
  }
})

fdk.discord(function(result){
  return {
    "content" : `The sentiment of the news article on a scale is: ${result.sentiment}`
  }
})
