'use strict'
const axios = require('axios')
const Promise = require('bluebird')

const sentimentAnalysis = (textInput, apiKey) => {
  let textArray
  if(Array.isArray(textInput)){
    textArray = textInput
  }else{
    textArray = [textInput]
  }

  return Promise.map(textArray, sentimentAnalysisUrl(apiKey))
}

const sentimentAnalysisUrl = (apiKey) => (text) => {
  const apiUrl = `https://gateway.watsonplatform.net/natural-language-understanding/api/v1/analyze?version=2018-11-16`
  const data = {
    text,
    features: {
      sentiment: {}
    }
  }

  const config = {
     auth: {
     username: 'apikey',
     password: apiKey
    }
  }

  return axios.post(apiUrl , data, config).then((result) => {
    return  result.data.sentiment.document.score
  }).catch((e)=>{
    return 0
  })
}

module.exports = sentimentAnalysis
