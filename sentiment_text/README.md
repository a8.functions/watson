# sentiment_text

Check sentiment of text or array of text

## Examples

### CLI

`a8 invoke watson.sentiment_text  '{"apiKey":"","text":"Today is a great day and everything is feeling amazing!"}'`

## Input

`apiKey` is a Watson API key

``` json
{
    apiKey : string,
    text: [array] or string
}
```

## Output

sentiments is an array of sentiments (from -1 to 1)

``` json
{
    sentiments : [array]
}
```